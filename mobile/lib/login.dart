import 'package:flutter/material.dart';
import 'after_login.dart';
//import 'problem_report.dart';

class LoginPage extends StatefulWidget {
	static String tag = 'loging_page';

	@override
	LoginState createState() {
		return LoginState();
	}
}

class LoginState extends State<LoginPage> {

	final GlobalKey<FormState> formKey = GlobalKey<FormState>();

	@override
	Widget build(BuildContext context) {
		final username = TextFormField(
			autofocus: false,
			decoration: InputDecoration(
				hintText: 'Username',
				contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
				border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
			),
			validator: (value) {
				if (value.isEmpty) {
					return 'Prease Enter Username';
				}
				if (value != 'customer1') {
					return 'No Username';
				}
			},
		);

		final password = TextFormField(
			autofocus: false,
			obscureText: true,
			decoration: InputDecoration(
				hintText: 'Password',
				contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
				border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
			),
			validator: (value) {
				if (value.isEmpty) {
					return 'Prease Enter Password';
				}
				if (value != 'password') {
					return 'Wrong Password';
				}
			},
		);

		final login_button = Padding(
			padding: EdgeInsets.symmetric(vertical: 16.0),
			child: Material(
				borderRadius: BorderRadius.circular(30.0),
				shadowColor: Colors.lightBlueAccent.shade100,
				elevation: 5.0,
				child: MaterialButton(
					minWidth: 200.0,
					height: 42.0,
					onPressed: () {
						if (formKey.currentState.validate()) {
							Navigator.push(
								context,
								MaterialPageRoute(builder: (context) => AfterLoginPage())
							);
						}
									//Navigartor.of(context).pushName(ProblemReportPage.tag);
					},
					color: Colors.lightBlueAccent,
					child: Text('Log In', style: TextStyle(color: Colors.white)),
				),
			),
		);

		return Scaffold(
			backgroundColor: Colors.white,
			body: Center(
				child: Form(
					key: formKey,
					child: ListView(
						shrinkWrap: true,
						padding: EdgeInsets.only(left: 24.0, right: 24.0),
						children: <Widget>[
							SizedBox(height: 48.0),
							username,
							SizedBox(height: 48.0),
							password,
							SizedBox(height: 48.0),
							login_button,
						],
					),
				),
			),
		);
	}
}

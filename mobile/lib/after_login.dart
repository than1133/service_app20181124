import 'package:flutter/material.dart';
import 'problem_report.dart';

class AfterLoginPage extends StatelessWidget {
  static String tag = 'after_login_page';

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(text: 'ผู้ใช้'),
              Tab(text: 'แจ้งปัญหา')
            ]
          ),
          title: Text('หน้าหลัก'),
        ),
        body: TabBarView(
          children: [
            Column(
              children: <Widget>[
                Text('Customer1'),
                Text('ที่อยู่: ทดสอบ'),
                Text('อีเมลล์: customer1@email.com'),
                Text('เบอร์โทร: 07778787777 '),
                Text('ตำแหน่ง: customer')
              ],
            ),
            ProblemReportPage(),
          ],
        ),
      ),
    );
  }
}
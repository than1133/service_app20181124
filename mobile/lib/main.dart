import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';

import 'login.dart';
//import 'problem_report.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  final routes = <String, WidgetBuilder> {
    LoginPage.tag: (context) => LoginPage(),
    //ProblemReportPage.tag: (context) => ProblemReportPage(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MainApp',
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
      routes: routes
    );
  }
}

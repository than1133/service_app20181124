import 'package:flutter/material.dart';
import 'problem_report.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      initialValue: 'user@email.com',
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(
          20.0,
          10,
          20.0,
          10.0,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            32.0
          ),
        ),
      ),    
    );

    final passworld = TextFormField(
      autofocus: false,
      initialValue: 'some password',
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(
          20.0,
          10.0,
          20.0,
          10.0,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            32.0,      
          ),      
        ),
      ),    
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(
          30.0,
        ),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.push(
              context, MaterialPageRoute(
                builder: (context) => TodoScreen(
                  todos: List.generate(
                    20,
                    (i) => Todo(
                      'รายการแจ้งซ้อมที่ $i',
                      'รายละเอียดรายการแจ้งซ้อมที่ $i'    
                    ),    
                  ),        
                ),
              ), 
            );
          },
          color: Colors.lightBlueAccent,
          child: Text('ล๊อกอิน',
            style: TextStyle(
             color: Colors.white
            ),
          ),    
        ),
      ),    
    );
    
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            SizedBox(height: 48.0),
            email,
            SizedBox(height: 8.0),
            passworld,
            SizedBox(height: 24.0),
            loginButton,
          ],    
        ),  
      ),    
    );
  }
}



import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';

class ProblemReportPage extends StatefulWidget {
  static String tag = 'problem_report_page';

  @override
  ProblemReportState createState() => ProblemReportState();
}

class ProblemReportState extends State<ProblemReportPage> {

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var problem_list = ['เช็คหัวพิมพ์', 'ซ่อมเครื่อง', 'เปลี่ยนอะไหล่', 'แก้ไขงานให้กับลูกค้า', 'ติดตั้งเครื่องใหม่'];
  var current_problem_select = 'เช็คหัวพิมพ์';
  Trouble trouble;
	String address_trouble = "troubles/";
  DatabaseReference itemRef;

  @override
  void initState() {
    super.initState();
    final now = new DateTime.now();
    trouble = Trouble(
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      ""
    );
    final FirebaseDatabase database = FirebaseDatabase.instance; //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child(address_trouble);
  }

  File image;

//  To use Gallery or File Manager to pick Image
//  Comment Line No. 19 and uncomment Line number 20
  picker() async {
    print('Picker is called');
    File img = await ImagePicker.pickImage(source: ImageSource.camera);
//    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (img != null) {
      image = img;
      setState(() {});
    }
  }

  void handleSubmit() {
    final FormState form = formKey.currentState;

    if (form.validate()) {
      form.save();
      form.reset();
      trouble.id = getDateSerial();
			setTroublePath(trouble.id);
			itemRef.set(trouble.toJson());
    }
  }

	void setTroublePath(trouble_id) {
		final FirebaseDatabase database = FirebaseDatabase.instance;
		itemRef = database.reference().child(address_trouble+trouble_id);
	}

	String getDateSerial() {
  	var now = new DateTime.now();
  	String nowString = now.toString();
  	return nowString.replaceAll(new RegExp(r'-* *:*'), '').split('.')[0];
	}

  @override
  Widget build(BuildContext context) {
    final problems = DropdownButton<String>(
      items: problem_list.map((String dropDownStringItem) {
        return DropdownMenuItem<String>(
          value: dropDownStringItem,
          child: Text(dropDownStringItem),
        );
      }).toList(),
      onChanged: (String new_value) {
        _onProblemSelected(new_value);
        trouble.trouble = new_value;
      },
      value: current_problem_select,
    );

    final detail = TextFormField(
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'รายละเอียด',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'ใส่รายละเอียด';
        }
      },
      onSaved: (value) => trouble.detail = value,
    );

    final open_image = Column(
      children: [
        MaterialButton(
          onPressed: picker,
          child: new Icon(Icons.camera_alt),
        ),
        Center(
          child: image == null
              ? new Text('No Image to Show ')
              : new Image.file(image),
        ),
      ],
    );

    final send_button = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            if (formKey.currentState.validate()) {
              handleSubmit();
            }
          },
          color: Colors.lightBlueAccent,
          child: Text('ส่ง', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    return Container(
      child: Form(
        key: formKey,
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: [
            Text('ปัญหา'),
            problems,
            Text('รายละเอียด'),
            detail,
            Text('เอกสาร'),
            Text('รูปภาพ'),
            open_image,
            Text('วีดีโอ'),
            send_button
          ],
        ),
      ),
    );
  }

  void _onProblemSelected(String value) {
    setState(() {
      this.current_problem_select = value;
    });
  }
}

class Trouble {
  String id;
  String customer;
  String detail;
  String date;
  String email;
  String operator;
  String status;
  String trouble;

  Trouble(
      this.id,
      this.customer,
      this.detail,
      this.date,
      this.email,
      this.operator,
      this.status,
      this.trouble
      );

  toJson() {
    var now = new DateTime.now();
    return {
      "id": id,
      "trouble": trouble,
      "date": now.toString(),
      "detail": detail,
      "attached_doc": '',
      "attached_pic": '',
      "attached_vid": '',
      "customer": 'customer1',
      "customer_username": 'customer1',
      "operator": "ไม่มี",
      "status": "ยังไม่ดำเนินการ",
      "email": ""
    };
  }
}

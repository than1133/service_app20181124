// Initialize Firebase
var config = {
  apiKey: "AIzaSyDUsLRZYDeaEVbjN9AGzUwBV1arT5wldFk",
  authDomain: "web20181124-eb78d.firebaseapp.com",
  databaseURL: "https://web20181124-eb78d.firebaseio.com",
  projectId: "web20181124-eb78d",
  storageBucket: "web20181124-eb78d.appspot.com",
  messagingSenderId: "576061274462"
};

firebase.initializeApp(config);

//hide_el('notic_user')
//hide_el('notic_pass')

var database = firebase.database();

// report problem to firebase
function fb_write_trouble(data) {
  database.ref('troubles/' + data.id).set(data);
}

// report completed problem to firebase
function fb_write_report(data) {
  database.ref('reports/'+data['serial']).set(data)
}

function fb_check_user() {
  var uu = get_el_value('login-username')
  var pp = get_el_value('login-password')
  console.log("get user")
  var ref = database.ref('Users');
  ref.orderByChild('username').equalTo(uu).once('value', function(snapshot) {
    // get user data
    user = snapshot.val() || 'Anonymous'
    console.log(user)
    if (user != 'Anonymous') {
      if (user[uu].password == pp) {
	      //hide_el('notic_pass')
	      app.login = true
	      to_loging(user[uu])
	      console.log("password valid")
      }
      else {
	      //show_el('notic_pass')
	      console.log("password invalid")
      }
    }
    else {
      //show_el('notic_user')
      console.log("check user invalid")
 
    }
  })
}

var storage_ref = firebase.storage()

function fb_upload_file(problem_id, file, name) {
  console.log('firebase upload')
  if (file != null) {
    storage_ref.ref(problem_id+'/'+name)
      .put(file).then(async function(snapshot){
        url = await fb_get_file_url(problem_id+'/'+name)
        console.log(problem_id+': '+url)
        return url
    })
  }
  else {
    console.log(problem_id+': null')
    return ''
  }
}

function fb_delete_data (trouble_id) {
  if (confirm("ต้องการลบ "+trouble_id+" ?")) {
    database.ref("troubles/"+trouble_id).remove() 
    app.remove_class_by_id('modal-editor', 'is-active')
  }
  else {
    
  }
}

function fb_get_file_url(file_name) {
  const url = storage_ref.ref(file_name).getDownloadURL()
  return url
}

// query problems
var problems = database.ref('troubles')
problems.orderByChild('customer').on('value', function(snapshot) {
  app.problems = snapshot.val()
  console.log(snapshot.val())
})


// query problems function
function query_problems() {
  var ref = database.ref('troubles')
  ref.orderByChild('customer').once('value', function(snapshot) {
    app.troubles = snapshot.val()
    console.log(snapshot.val())
  })
}

// query problems with operator
function operator_problems (username) {
  problems.orderByChild('operator').equalTo(username)
    .on('value', function(snapshot) {
      app.operator_problems = snapshot.val()
      console.log(snapshot.val())
    })
}

problems.orderByChild('operator').equalTo(get_full_name())
  .on('value', function(snapshot) {
    app.operator_problems = snapshot.val()
    console.log(snapshot.val())
  })



// query operators
var operators = database.ref('Users')
operators.orderByChild('role').equalTo('operator')
  .on('value', function (snapshot) {
    console.log(snapshot.val())
    app.operators = snapshot.val()
  })

function update_trouble_operator(problem, operator_name) {
  var trouble_operator = database.ref('troubles')
  trouble_operator.child(problem).child('operator').set(operator_name)
}

function update_trouble_status(problem, _status) {
  var trouble_operator = database.ref('troubles')
  trouble_operator.child(problem).child('status').set(_status)
}

function fb_update_trouble_all (trouble_id, data) {
  if (confirm("ต้องการแก้ไข "+trouble_id+" ?")) {
    problems.child(trouble_id).set(data)
    app.remove_class_by_id('modal-editor', 'is-active')
  }
  else {
    
  }
}

function get_full_name() {
  return localStorage.getItem('full_name')
}

function get_username() {
  return localStorage.getItem('username')
}

function hide_all_content() {
  hide_el('hire-to-fix')
  hide_el('operator-tracking')
  hide_el('profile')
  hide_el('repair-jobs')
  hide_el('job-report')
  hide_el('login')
  hide_el('register')
  hide_el('problem-report')
}

function to_loging(data) {
  localStorage.setItem('is_logged_in', 'true')
  localStorage.setItem('address', data.address)
  localStorage.setItem('email', data.email)
  localStorage.setItem('full_name', data.full_name)
  localStorage.setItem('role', data.role)
  localStorage.setItem('tel', data.tel)
  localStorage.setItem('username', data.username)
  location.reload(); 
}

// clear user data when logout
function logout() {
  localStorage.setItem('is_logged_in', 'false');
  localStorage.clear()
   location.reload(); 

}

function fb_register(d) {
  database.ref('Users/' + d.username).set(d);
  location.reload(); 
}


function get_el_id(name) {
  var el = document.getElementById(name)
  return el
}

function get_el_value(name) {
  var el = get_el_id(name)
  return el.value
}

function get_el_input_file(name) {
  var el = get_el_id(name)
  var file = el.files[0]
  return file
}

function show_el(name) {
  el = get_el_id(name)
  el.style.display = "block"
}

function hide_el(name) {
  el = get_el_id(name)
  el.style.display = "none"
}

Vue.component('user-profile', {
  props: ['user'],
  template: `
  <div>
    <section class="hero">
      <div class="hero-body">
        <div class="container">
  	<h1 class="title">
  	  {{ user.full_name }}
  	</h1>
  	<h2 class="subtitle">
  	  {{ user.username }}
  	</h2>
        </div>
      </div>
    </section>
    <div class="columns">
      <div class="column is-6 is-offset-1">
        <div>
	  <strong>ที่อยู่:</strong> {{user.address}}
        </div>
        <br><br>
        <div>
	  <strong>อีเมลล์:</strong> {{user.email}}
        </div>
        <br><br>
        <div>
	  <strong>เบอร์โทร:</strong> {{user.tel}}
        </div>
        <br><br>
        <div>
	  <strong>ตำแหน่ง:</strong> {{user.role}}
        </div>
      </div>
    </div>
  </div>
  `
})

Vue.component('problem-item', {
  props: ['problem'],
  template:`
  <tr>
    <td>{{ problem.id }}</td>
    <td>{{ problem.date }}</td>
    <td>{{ problem.customer }}</td>
    <td>{{ problem.trouble }}</td>
    <td>
      {{ problem.operator}}
      <br>
      <a
	v-on:click="$emit('click_edit', problem.id)"
      >แก้ไข</a>      
    </td>
    <td>{{ problem.status }}</td>
    <td>{{ problem.detail }}</td>
    <td>
      <a
        v-on:click="$emit('click_editor', problem.id)" 
      >แก้ไข</a>
    </td>
  </tr>
  `
})

Vue.component('operator', {
  props: ['problem', 'operator_name'],
  template: `
  <button 
    v-on:click="$emit('bclick', [problem, operator_name])"
    >
    {{ operator_name }}
  </button>
  `
})

Vue.component('repair-job-items', {
  props: ['problem'],
  template:`
  <tr>
    <td>{{ problem.id }}</td>
    <td>{{ problem.date }}</td>
    <td>{{ problem.customer }}</td>
    <td>{{ problem.trouble }}</td>
    <td>
      {{ problem.operator}}     
    </td>
    <td>
      {{ problem.status }}
      <br>
      <status-choice
      v-on:click_finish="$emit('send_update', [problem.id, 'เสร็จสิ้น'])"

      v-on:click_unfinish="$emit('send_update', [problem.id, 'ยังไม่ดำเนินการ'])">
         </status-choice>

    </td>
    <td>{{ problem.detail }}</td>
    <td>
      <a
        v-on:click="$emit('click_editor', problem.id)" 
      >แก้ไข</a>
    </td>
  </tr>
    `
})

Vue.component('status-choice', {
  template: `
  <div>
    <button
      v-on:click="$emit('click_unfinish')"> 
      ยังไม่ดำเนินการ
    </button>
    <button
      v-on:click="$emit('click_finish')"> 
      เสร็จสิ้น
    </button>
  </div>
  `
})

var app = new Vue({
  el: '#app',
  data: {
    login: false,
    user: {
      username: 'username',
      address: 'address',
      email: 'email',
      role: 'role',
      full_name: 'full_name',
      tel: 'tel',
      admin_search: '',
      operator_search: '',
    },
    operators: [],
    operator_problems: [],
    problems: [],
    current_id_trouble_operator_edit: '0',
    current_trouble_editor: '',
    editor: {
      customer: '',
      customer_username: '',
      date: '',
      operator: '',
      detail: '',
      status_selected: '',
      problem: '',
      doc: '',
      pic: '',
      vid: '',
    },
  },
  watch: {
    current_trouble_editor: function() {
      this.editor.customer = this.problems[this.current_trouble_editor].customer
      this.edit.customer_username = this.problems[this.current_touble_editor].customer_username
      this.editor.operator = this.problems[this.current_trouble_editor].operator
      this.editor.date = this.problems[this.current_trouble_editor].date
      this.editor.detail = this.problems[this.current_trouble_editor].detail
      this.editor.status_selected = this.problems[this.current_trouble_editor].status
      this.editor.problem = this.problems[this.current_trouble_editor].trouble
      this.editor.doc = this.problems[this.current_trouble_editor].doc
      if (this.editor.doc) {
        this.editor.doc = " "
      }
      this.editor.pic = this.problems[this.current_trouble_editor].pic
      if (this.editor.pic) {
        this.editor.doc = " "
      }
      this.editor.vid = this.problems[this.current_trouble_editor].vid
      if (this.editor.vid) {
        this.editor.doc = " "
      }
      console.log(this.editor)
    }, 
  },
  computed: {
    editor_status_selected: function() {
      return this.problems[this.current_trouble_editor].status
    },
  },
  created() {
    this.login = (localStorage.getItem('is_logged_in') == 'true')
    this.user.address = localStorage.getItem('address')
    this.user.email = localStorage.getItem('email')
    this.user.full_name = localStorage.getItem('full_name')
    this.user.role = localStorage.getItem('role')
    this.user.tel = localStorage.getItem('tel')
    this.user.username = localStorage.getItem('username')
    hide_all_content()
    if (this.login == true) {
      show_el('profile')
    }
    else {
      show_el('login')
      show_el('register')
    } 
  },
  methods: {
     send_trouble: function() {
      console.log('click send')
      if (this.login == false) {
	      this.add_class_by_id('modal_login','is-active')
	      console.log('non login')
      }
      else {
        date_now = Date.now();
        doc_file = fb_upload_file(
          date_now,
          get_el_input_file('troub_doc'),
          "doc"
        )
        pic_file = fb_upload_file(
          date_now, 
          get_el_input_file('troub_pic'),
          "pic"
        )
        vid_file = fb_upload_file(
          date_now, 
          get_el_input_file('troub_vid'),
          "vid"
        )
        data = {
            id: date_now,
            trouble: get_el_value('troub_trouble'),
            date: get_el_value('troub_date'),
            detail: get_el_value('troub_detail'),
            attached_doc: '',
            attached_pic: '',
            attached_vid: '',
            customer: get_full_name(),
            customer_username: get_username(),
            operator: "ไม่มี",
            status: "ยังไม่ดำเนินการ",
            email: ""
          }
          
          if (data.date != '') {
            console.log('send ok')
            fb_write_trouble(data)
          }
      }
    },
    registering: function( ){
      var reg_dat = {
	      full_name: get_el_value('register-full_name'),
	      address: get_el_value('register-address'),
	      tel: get_el_value('register-tel'),
	      username: get_el_value('register-username'),
	      email: get_el_value('register-email'),
	      password: get_el_value('register-password'),
	      role: 'customer'
      }
      fb_register(reg_dat)
    },
    click_content: function(content) {
      hide_all_content()
      show_el(content)
    },
    logingin: function() {
      this.login = !this.login
    },
    add_class_by_id: function(id, _class) {
      console.log("add class "+_class+" from "+id)
      var el = get_el_id(id)
      el.classList.add(_class)
    },
    remove_class_by_id: function(id, _class) {
      console.log("remove class "+_class+" from "+id)
      var el = get_el_id(id)
      el.classList.remove(_class)
    },
    focus_table: function(dir) {
      $('#'+dir).focus()
    },
    loging_in: function(){
      console.log("press submit login")
      fb_check_user()
    },
    logout: function(){
      logout()
      this.login = false
      console.log("press logout")
    },
    change_tab: function(num_tab) {
      // reset tab
      this.remove_class_by_id("tab"+1, 'is-active')
      this.remove_class_by_id("tab"+2, 'is-active')
      this.remove_class_by_id("tab"+3, 'is-active')

      // active tab
      this.add_class_by_id("tab"+num_tab, 'is-active')

      // reset conect
      hide_el("data_content"+1)
      hide_el("data_content"+2)
      hide_el("data_content"+3)
      // show content
      show_el("data_content"+num_tab)
    },
    delete_data_firebase: function (trouble_id) {
      fb_delete_data(trouble_id)
    },
    click_edit_trouble_all: function (val){
      this.current_trouble_editor = val
      this.add_class_by_id('modal-editor','is-active')
    },
    click_edit_trouble_operator : function(val){
      this.add_class_by_id('modal-operators', 'is-active')
      this.current_id_trouble_operator_edit = val
    },
    update_trouble_all: function (trouble_id) {
      data = {
            id: this.current_trouble_editor,
            trouble: this.editor.problem,
            date: this.editor.date,
            detail: this.editor.detail,
            attached_doc: '',
            attached_pic: '',
            attached_vid: '',
            customer: this.editor.customer,
            customer_username: this.editor.customer_username,  
            operator: this.editor.operator,
            status: this.editor.status_selected,
            email: ""
          }
      console.log(data)
      fb_update_trouble_all(trouble_id, data) 
    },
    update_trouble_operator: function (val) {
      this.remove_class_by_id('modal-operators', 'is-active')
      update_trouble_operator(this.current_id_trouble_operator_edit, val)
    },
    update_status: function (val) {
      update_trouble_status(val[0], val[1])
    },
    save_report: function () {
      report_data = {
	      full_name: get_el_value('report-full_name'),
	      company: get_el_value('report-company'),
	      serial: get_el_value('report-serial'),
	      tel: get_el_value('report-tel'),
	      address: get_el_value('report-address'),
	      signature: get_el_value('report-signature'),
	      responsible_person: get_el_value('report-responsible_person'),
	      data: get_el_value('report-date')
      }
      fb_write_report(report_data)
    }
  },
})

